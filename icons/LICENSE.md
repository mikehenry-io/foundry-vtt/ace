# Artwork License

The Advanced Cepheus Engine (ACE) game system for Foundry Virtual Tabletop includes icon artwork licensed from Historical Battlefields, Sketchfab, and others. Attributions listed below.

These icons are packaged with and provided for use with the ___ACE___ game system and may not be redistributed or used outside of Foundry Virtual Tabletop except as permitted by their respective licenses, as noted below.

# Historical Battlefields - Token Builder

Historical Battlefields -- https://www.patreon.com/Grantovich/ -- https://hbtokenbuilder.ru/

Thumbnails and images used with expressed permission by the author.

# Sketchfab.com Polygonal Modern Weapons Asset Package

Aligned Games -- https://alignedgames.com/ -- alignedgames@mailbox.co.za

https://sketchfab.com/3d-models/polygonal-modern-weapons-asset-package-a35fc24f512e448ebed403beffe09c0d

CC Attribution 4.0 International (CC BY 4.0) -- https://creativecommons.org/licenses/by/4.0/