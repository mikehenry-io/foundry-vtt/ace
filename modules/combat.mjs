/**
 * Override the default Initiative formula to customize special behaviors of the system.
 * Apply advantage, proficiency, or bonuses where appropriate
 * Apply the dexterity score as a decimal tiebreaker if requested
 * See Combat._getInitiativeFormula for more detail.
 * @returns {string}  Final initiative formula for the actor.
 */
export default function getInitiativeFormula() {
  const actor = this.actor;
  if ( !actor ) return "2d6";

  const initiative = actor.system.attributes.initiative;
  const rollData = actor.getRollData();

  // Construct initiative formula parts
  let nd = 2;
  let mods = "";

  if ( actor.getFlag("ACE", "initiativeAdv") ) {
    nd = 3;
    mods += "kh";
  }

  const parts = [
    `${nd}d6${mods}`,
    initiative.mod,
    (initiative.prof.term !== "0") ? initiative.prof.term : null,
    (initiative.bonus !== 0) ? initiative.bonus : null
  ];

  return parts.filter(p => p !== null).join(" + ");
};
