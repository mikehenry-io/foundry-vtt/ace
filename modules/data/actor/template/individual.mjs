import ACE_CommonTemplate from "./common.mjs";

export default class ACE_IndiviualTemplate extends
  ACE_CommonTemplate
{

  /** @inheritdoc */
  static _systemType = "individual";

  /* -------------------------------------------- */

  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    // ACE_IndiviualTemplate.#migrateSensesData(source);
  }
}
