import ACE_IndividualTemplate from "./templates/individual.mjs";

export default class ACE_CharacterData extends
  ACE_IndividualTemplate
{

  /** @inheritdoc */
  static _systemType = "character";

  /* -------------------------------------------- */

  /** @inheritdoc */
  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    // AttributesFields._migrateInitiative(source.attributes);
    return super.migrateData(source);
  }
}
