/**
 * A type of Roll specific to a 2d6-based check in the A·C·E system.
 * @param {string} formula                       The string formula to parse
 * @param {object} data                          The data object against which to parse attributes within the formula
 * @param {object} [options={}]                  Extra optional arguments which describe or modify the Check
 * @param {number} [options.advantageMode]       What advantage modifier to apply to the roll (none, advantage,
 *                                               disadvantage)
 * @param {number} [options.exc_success=5]       The value of the check result which represents an exceptional success
 * @param {number} [options.exc_failure=-5]      The value of the check result which represents an exceptional failure
 * @param {(number)} [options.targetValue=7]     Assign a target value against which the result of this roll should be
 *                                               compared
 */
export default class ACE_Check extends Roll {
  constructor(formula, data, options) {
    super(formula, data, options);
    if (!this.options.configured) this.configureModifiers();
  }

  /* -------------------------------------------- */

  /**
   * Create a Check from a standard Roll instance.
   * @param {Roll} roll
   * @returns {Check}
   */
  static fromRoll(roll) {
    const newRoll = new this(roll.formula, roll.data, roll.options);
    Object.assign(newRoll, roll);
    return newRoll;
  }

  /* -------------------------------------------- */

  /**
   * Determine whether a d20 roll should be fast-forwarded, and whether advantage or disadvantage should be applied.
   * @param {object} [options]
   * @param {Event} [options.event]                               The Event that triggered the roll.
   * @param {boolean} [options.advantage]                         Is something granting this roll advantage?
   * @param {boolean} [options.disadvantage]                      Is something granting this roll disadvantage?
   * @param {boolean} [options.fastForward]                       Should the roll dialog be skipped?
   * @returns {{advantageMode: D20Roll.ADV_MODE, isFF: boolean}}  Whether the roll is fast-forwarded, and its advantage
   *                                                              mode.
   */
  static determineAdvantageMode({event, advantage=false, disadvantage=false, fastForward}={}) {
    const isFF = fastForward ?? (event?.shiftKey || event?.altKey || event?.ctrlKey || event?.metaKey);
    let advantageMode = this.ADV_MODE.NORMAL;
    if ( advantage || event?.altKey ) advantageMode = this.ADV_MODE.ADVANTAGE;
    else if ( disadvantage || event?.ctrlKey || event?.metaKey ) advantageMode = this.ADV_MODE.DISADVANTAGE;
    return {isFF: !!isFF, advantageMode};
  }

  /* -------------------------------------------- */

  /**
   * Advantage mode of a check
   * @enum {number}
   */
  static ADV_MODE = {
    NORMAL: 0,
    ADVANTAGE: 1,
    DISADVANTAGE: -1
  };

  /* -------------------------------------------- */

  /**
   * The HTML template path used to configure evaluation of this Roll
   * @type {string}
   */
  static EVALUATION_TEMPLATE = "systems/ACE/templates/chat/roll-dialog.hbs";

  /* -------------------------------------------- */

  /**
   * Does this roll start with a d20?
   * @type {boolean}
   */
  get validCheck() {
    return (this.terms[0] instanceof Die)
      && (this.terms[0].faces === 6)
      && (this.terms[0].value === 2)
      && (this.terms[1].expression === "-")
      && (this.terms[2].number === 7);
  }

  /* -------------------------------------------- */

  /**
   * A convenience reference for whether this Check has advantage
   * @type {boolean}
   */
  get hasAdvantage() {
    return this.options.advantageMode === ACE_Check.ADV_MODE.ADVANTAGE;
  }

  /* -------------------------------------------- */

  /**
   * A convenience reference for whether this Check has disadvantage
   * @type {boolean}
   */
  get hasDisadvantage() {
    return this.options.advantageMode === ACE_Check.ADV_MODE.DISADVANTAGE;
  }

  /* -------------------------------------------- */

  /**
   * Is this roll an exceptional success? Returns undefined if roll isn't evaluated.
   * @type {boolean|void}
   */
  get isExceptionalSuccess() {
    if ( !this.validCheck || !this._evaluated ) return undefined;
    if ( !Number.isNumeric(this.options.exc_success) ) return false;
    return this.dice[0].total > this.options.exc_success;
  }

  /* -------------------------------------------- */

  /**
   * Is this roll a exceptional failure? Returns undefined if roll isn't evaluated.
   * @type {boolean|void}
   */
  get isExceptionalFailure() {
    if ( !this.validCheck || !this._evaluated ) return undefined;
    if ( !Number.isNumeric(this.options.exc_failure) ) return false;
    return this.dice[0].total < this.options.exc_failure;
  }

  /* -------------------------------------------- */
  /*  Effect Roll Methods                            */
  /* -------------------------------------------- */

  /**
   * Apply optional modifiers which customize the behavior of the d20term
   * @private
   */
  configureModifiers() {
    if ( !this.validCheck ) return;

    const term = this.terms[0];
    term.modifiers = [];

    // Handle Advantage or Disadvantage
    if ( this.hasAdvantage ) {
      term.number = 3;
      term.modifiers.push("kh2");
      term.options.advantage = true;
    }
    else if ( this.hasDisadvantage ) {
      term.number = 3;
      term.modifiers.push("kl2");
      term.options.disadvantage = true;
    }
    else term.number = 2;

    // Assign exceptional and fumble thresholds
    if ( this.options.exc_success ) term.options.exc_success = this.options.exc_success;
    if ( this.options.exc_failure ) term.options.exc_failure = this.options.exc_failure;
    if ( this.options.targetValue ) term.options.target = this.options.targetValue;

    // Re-compile the underlying formula
    this._formula = this.constructor.getFormula(this.terms);

    // Mark configuration as complete
    this.options.configured = true;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async toMessage(messageData={}, options={}) {

    // Evaluate the roll now so we have the results available to determine whether reliable talent came into play
    if ( !this._evaluated ) await this.evaluate({async: true});

    // Add appropriate advantage mode message flavor and ace roll flags
    messageData.flavor = messageData.flavor || this.options.flavor;
    if ( this.hasAdvantage ) messageData.flavor += ` (${game.i18n.localize("ACE.Advantage")})`;
    else if ( this.hasDisadvantage ) messageData.flavor += ` (${game.i18n.localize("ACE.Disadvantage")})`;

    // // Add reliable talent to the d20-term flavor text if it applied
    // if ( this.validCheck && this.options.reliableTalent ) {
    //   const d20 = this.dice[0];
    //   const isRT = d20.results.every(r => !r.active || (r.result < 10));
    //   const label = `(${game.i18n.localize("ACE.FlagsReliableTalent")})`;
    //   if ( isRT ) d20.options.flavor = d20.options.flavor ? `${d20.options.flavor} (${label})` : label;
    // }

    // Record the preferred rollMode
    options.rollMode = options.rollMode ?? this.options.rollMode;
    return super.toMessage(messageData, options);
  }

  /* -------------------------------------------- */
  /*  Configuration Dialog                        */
  /* -------------------------------------------- */

  /**
   * Create a Dialog prompt used to configure evaluation of an existing ACE_Check instance.
   * @param {object} data                     Dialog configuration data
   * @param {string} [data.title]             The title of the shown dialog window
   * @param {number} [data.defaultRollMode]   The roll mode that the roll mode select element should default to
   * @param {number} [data.defaultAction]     The button marked as default
   * @param {boolean} [data.chooseModifier]   Choose which characteristic modifier should be applied to the roll?
   * @param {string} [data.defaultCharacteristic]   For tool rolls, the default characteristic modifier applied to the
   *                                                roll
   * @param {string} [data.template]          A custom path to an HTML template to use instead of the default
   * @param {object} options                  Additional Dialog customization options
   * @returns {Promise<ACE_Check|null>}       A resulting ACE_Check object constructed with the dialog, or null if the
   *                                          dialog was closed
   */
  async configureDialog({title, defaultRollMode, defaultAction=ACE_Check.ADV_MODE.NORMAL, chooseModifier=false,
    defaultCharacteristic, template}={}, options={}) {

    // Render the Dialog inner HTML
    const content = await renderTemplate(template ?? this.constructor.EVALUATION_TEMPLATE, {
      formula: `${this.formula} + @bonus`,
      defaultRollMode,
      rollModes: CONFIG.Dice.rollModes,
      chooseModifier,
      defaultCharacteristic,
      characteristics: CONFIG.ACE.characteristics
    });

    let defaultButton = "normal";
    switch ( defaultAction ) {
      case ACE_Check.ADV_MODE.ADVANTAGE: defaultButton = "advantage"; break;
      case ACE_Check.ADV_MODE.DISADVANTAGE: defaultButton = "disadvantage"; break;
    }

    // Create the Dialog window and await submission of the form
    return new Promise(resolve => {
      new Dialog({
        title,
        content,
        buttons: {
          advantage: {
            label: game.i18n.localize("ACE.Advantage"),
            callback: html => resolve(this._onDialogSubmit(html, ACE_Check.ADV_MODE.ADVANTAGE))
          },
          normal: {
            label: game.i18n.localize("ACE.Normal"),
            callback: html => resolve(this._onDialogSubmit(html, ACE_Check.ADV_MODE.NORMAL))
          },
          disadvantage: {
            label: game.i18n.localize("ACE.Disadvantage"),
            callback: html => resolve(this._onDialogSubmit(html, ACE_Check.ADV_MODE.DISADVANTAGE))
          }
        },
        default: defaultButton,
        close: () => resolve(null)
      }, options).render(true);
    });
  }

  /* -------------------------------------------- */

  /**
   * Handle submission of the Roll evaluation configuration Dialog
   * @param {jQuery} html            The submitted dialog content
   * @param {number} advantageMode   The chosen advantage mode
   * @returns {ACE_Check}            This damage roll.
   * @private
   */
  _onDialogSubmit(html, advantageMode) {
    const form = html[0].querySelector("form");

    // Append a situational bonus term
    if ( form.bonus.value ) {
      const bonus = new Roll(form.bonus.value, this.data);
      if ( !(bonus.terms[0] instanceof OperatorTerm) ) this.terms.push(new OperatorTerm({operator: "+"}));
      this.terms = this.terms.concat(bonus.terms);
    }

    // Customize the modifier
    if ( form.characteristic?.value ) {
      const characteristic = this.data.characteristics[form.characteristic.value];
      this.terms = this.terms.flatMap(t => {
        if ( t.term === "@mod" ) return new NumericTerm({number: characteristic.mod});
        if ( t.term === "@characteristicCheckModifier" ) {
          const bonus = characteristic.bonuses?.check;
          if ( bonus ) return new Roll(bonus, this.data).terms;
          return new NumericTerm({number: 0});
        }
        return t;
      });
      this.options.flavor += ` (${CONFIG.ACE.characteristics[form.characteristic.value]})`;
    }

    // Apply advantage or disadvantage
    this.options.advantageMode = advantageMode;
    this.options.rollMode = form.rollMode.value;
    this.configureModifiers();

    return this;
  }
}
