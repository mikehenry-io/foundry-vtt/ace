import ACE from "../config.mjs";
// Import { ACE_Item } from "../item/entity.mjs";

/**
 * Extend the base Actor class to implement additional system-specific logic.
 * @extends {Actor}
 */
export class ACE_Actor extends Actor {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * The Actor's currently equipped armor, if any.
   * @type {ACE_Item|null}
   */
  get armor() {
    return this.system.attributes.armor.equippedArmor ?? null;
  }

  /* -------------------------------------------- */

  /**
   * The Actor's currently equipped shield, if any.
   * @type {ACE_Item|null}
   */
  get shield() {
    return this.system.attributes.armor.equippedShield ?? null;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /** @override */
  prepareData() {
    this._preparationWarnings = [];
    super.prepareData();

    // Iterate over owned items and recompute attributes that depend on prepared actor data
    this.items.forEach(item => item.prepareFinalAttributes());

    // Console.log(`A·C·E | ACE_Actor.prepareData(): ${this.name} [${this.id}]`);
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  prepareBaseData() {
    // Call "_prepare<Type>Data()" method depending on concrete type, if it exists
    const type = this.type.charAt(0).toUpperCase() + this.type.slice(1);
    const fn = this[`_prepare${type}Data`];
    if (fn) fn.bind(this)();
  }

  /* --------------------------------------------- */

  /** @inheritDoc */
  applyActiveEffects() {
    // The Active Effects do not have access to their parent at preparation time, so we wait until this stage to
    // determine whether they are suppressed or not.
    this.effects.forEach(e => e.determineSuppression());
    return super.applyActiveEffects();
  }

  /* --------------------------------------------- */

  /** @override */
  prepareDerivedData() {
    let upp = "";

    for (let [id, characteristic] of Object.entries(this.system.characteristics)) {
      characteristic.value = Math.min(characteristic.value, characteristic.max);
      characteristic.mod = Math.floor((characteristic.value / 3) - 2);

      upp += this._toTetratrigesimal(characteristic.max);
    }

    // Set character's total armor value
    this.system.attributes.armor_value =
      Object.values(this.system.attributes.armor).reduce((a, b) => a + b, 0);

    // Set character's move
    this.system.attributes.movement.walk =
      1.5 * (4 + this.system.characteristics.dex.mod);

    // Set character's health max and current value
    this.system.attributes.hits.max =
      this.system.characteristics.str.max
      + this.system.characteristics.dex.max
      + this.system.characteristics.end.max;

    this.system.attributes.hits.value =
      this.system.characteristics.str.value
      + this.system.characteristics.dex.value
      + this.system.characteristics.end.value;

    // Set character's UPP
    this.system.attributes.upp = upp.toUpperCase();

    // Inventory encumbrance
    this.system.attributes.encumbrance = this._computeEncumbrance();
  }

  /* -------------------------------------------- */

  /**
   * @inheritdoc
   * @param {object} [options]
   * @param {boolean} [options.deterministic] Whether to force deterministic values for data properties that could be
   *                                            either a die term or a flat term.
   */
  getRollData({ deterministic=false }={}) {
    const data = foundry.utils.deepClone(super.getRollData());
    // Data.prof = new Proficiency(this.system.attributes.prof, 1);
    // if ( deterministic ) data.prof = data.prof.flat;

    // data.classes = {};
    // for ( const [identifier, cls] of Object.entries(this.classes) ) {
    //   data.classes[identifier] = cls.system;
    //   if ( cls.subclass ) data.classes[identifier].subclass = cls.subclass.system;
    // }
    return data;
  }

  /* -------------------------------------------- */
  /*  Base Data Preparation Helpers               */
  /* -------------------------------------------- */

  /**
   * Update the actor's characteristics list to match the characteristics configured in `ACE.characteristics`.
   * Mutates the system.characteristics object.
   * @param {object} updates  Updates to be applied to the actor. *Will be mutated.*
   * @protected
   */
  _prepareBaseCharacteristics(updates) {
  }

  /* -------------------------------------------- */
  /*  Data Preparation Helpers                    */
  /* -------------------------------------------- */

  /**
   * Compute the level and percentage of encumbrance for an Actor.
   *
   * Optionally include the weight of carried currency across all denominations by applying the standard rule
   * from the PHB pg. 143
   * @returns {{max: number, value: number, pct: number}}  An object describing the character's encumbrance level
   * @private
   */
  _computeEncumbrance() {
    // Get the total weight from items
    const physicalItems = ["weapon", "equipment", "consumable", "tool", "backpack", "loot"];
    let weight = this.items.reduce((weight, i) => {
      if (!physicalItems.includes(i.type)) return weight;
      const q = i.system.quantity || 0;
      const w = i.system.weight || 0;
      return weight + (q * w);
    }, 0);

    // Determine the encumbrance size class
    let loc = ACE.locoWeights[this.system.attributes.loco] || "bipedal";
    let mod = loc[this.system.attributes.size] || 1;

    // Compute Encumbrance percentage
    weight = weight.toNearest(0.01);

    const max = Math.floor((this.system.characteristics.str.value ** 2 / 4 + 4) * 3 * mod);
    const pct = Math.clamped(100 * weight / max, 0, 100);
    return {
      color: `hsl(${1.2*(100-pct)}, 100%, 40%)`,
      encumbered: pct > (200 / 3),
      max,
      pct,
      value: weight.toNearest(0.1)
    };
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);
    const sourceId = this.getFlag("core", "sourceId");
    if (sourceId?.startsWith("Compendium.")) return;

    // Some sensible defaults for convenience
    // Token size category
    const s = CONFIG.ACE.tokenSizes[this.system.attributes.size || "med"];
    const prototypeToken = { width: s, height: s };

    // Player character configuration
    if (this.type === "character") Object.assign(prototypeToken, { vision: true, actorLink: true, disposition: 1 });

    this.updateSource({ prototypeToken });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _preUpdate(changed, options, user) {
    await super._preUpdate(changed, options, user);

    // Apply changes in Actor size to Token width/height
    const newSize = foundry.utils.getProperty(changed, "system.attributes.size");
    if (newSize && (newSize !== foundry.utils.getProperty(this.data, "system.attributes.size"))) {
      let size = CONFIG.ACE.tokenSizes[newSize];
      if (!foundry.utils.hasProperty(changed, "token.width")) {
        changed.token = changed.token || {};
        changed.token.height = size;
        changed.token.width = size;
      }
    }

    // Reset death save counters
    // const isDead = this.system.attributes.hp.value <= 0;
    // if ( isDead && (foundry.utils.getProperty(changed, "system.attributes.hp.value") > 0) ) {
    //   foundry.utils.setProperty(changed, "system.attributes.death.success", 0);
    //   foundry.utils.setProperty(changed, "system.attributes.death.failure", 0);
    // }
  }

  /* -------------------------------------------- */
  /*  Gameplay Mechanics                          */
  /* -------------------------------------------- */

  /**
   * Roll a Skill Check
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param {string} skillId      The skill id (e.g. "ins")
   * @param {object} options      Options which configure how the skill check is rolled
   * @returns {Promise<ACE_Roll>}  A Promise which resolves to the created Roll instance
   */
  async rollSkill(skillId, options = {}) {
    const skill = [...this.items].filter(i => i.type === "skill" && i._id === skillId);
    // Const skill = [...this.items].filter(i => i.type === "skill")[skillId];

    console.log(`A·C·E | ACE_Actor.rollSkill(skillId=${skillId}, options=${options}):`, this, skill);

    // // Ensure the options object is ready
    // options = foundry.utils.mergeObject({
    //   configureDialog: true,
    //   createMessage: true,
    //   flags: {}
    // }, options);

    // const cardData = await skill.displayCard(options);

    // Create or return the Chat Message data
    return skill.use({}, options);
  }

  /* -------------------------------------------- */

  /**
   * Roll a generic characteristic test or saving throw.
   * Prompt the user for input on which variety of roll they want to do.
   * @param {string} characteristicId    The characteristic id (e.g. "str")
   * @param {object} options      Options which configure how characteristic tests or saving throws are rolled
   */
  rollCharacteristic(characteristicId, options = {}) {
    console.log(`A·C·E | ACE_Actor.rollCharacteristic(characteristicId=${characteristicId}, options=${options})`);
  }

  /* -------------------------------------------- */
  /*  Helper Functions                            */
  /* -------------------------------------------- */

  /**
   * Convert a decimal number to a single tetratrigesimal digit.
   *
   * @param {number}    A decimal number between 0 and 33 (inclusive).
   * @param decimal
   * @returns {string}  The converted tetratrigesimal digit.
   */
  _toTetratrigesimal(decimal) {
    return ACE.tetratrigesimal[decimal];
  }

  /**
   * Convert a single tetratrigesimal digit to a decimal number.
   *
   * @param {string} tetratrigesimal  The tetratrigesimal digit to convert to a decimal number.
   * @returns {number}                The converted decimal number.
   */
  _fromTetratrigesimal(tetratrigesimal) {
    return ACE.tetratrigesimal.indexOf(tetratrigesimal);
  }

  /* -------------------------------------------- */
}
