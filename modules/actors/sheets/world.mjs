import { ACE_ActorSheet } from "./actor.mjs";

/**
 * An Actor sheet for npc type actors.
 * @extends {ACE_ActorSheet}
 */
export class ACE_WorldSheet extends ACE_ActorSheet {
  /**
   * Define default rendering options for the NPC sheet.
   * @returns {object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "actor", "world"],
      height: 645
    });
  }

  // --------------------------------------------

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   * @param {object} options
   * @returns {object}  Prepared copy of the actor data ready to be displayed.
   */
  async getData(options={}) {
    return mergeObject(await super.getData(options), {
      isWorld: true
    });
  }
}
