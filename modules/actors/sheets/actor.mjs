// Import * as ACE from "../../config.mjs";
// import { ACE_Actor } from "../entity.mjs";
// import { ACE_Item } from "../../item/entity.mjs";
import ActorMovementConfig from "./movement-config.mjs";

/**
 * Extend the basic ActorSheet class to suppose system-specific logic and functionality.
 * @abstract
 * @extends {ActorSheet}
 */
export class ACE_ActorSheet extends ActorSheet {

  /**
   * Track the set of item filters which are applied
   * @type {Object<string, Set>}
   * @protected
   */
  _filters = {
    inventory: new Set(),
    skills: new Set()
  };

  /* -------------------------------------------- */

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "actor"],
      scrollY: [".inventory .inventory-list"],
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "description"
      }],
      width: 720
    });
  }

  /* -------------------------------------------- */

  /**
   * A set of item types that should be prevented from being dropped on this type of actor sheet.
   * @type {Set<string>}
   */
  static unsupportedItemTypes = new Set();

  /* -------------------------------------------- */

  /** @override */
  get template() {
    if (!game.user.isGM && this.actor.limited) {
      return "systems/ACE/templates/actors/limited.hbs";
    }

    return `systems/ACE/templates/actors/${this.actor.type}.hbs`;
  }

  /* -------------------------------------------- */
  /*  Context Preparation                         */
  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    // The Actor's data
    const source = this.actor.toObject();

    // Basic data
    const context = {
      actor: this.actor,
      source: source.system,
      system: this.actor.system,
      items: Array.from(this.actor.items),
      itemContext: {},
      characteristics: foundry.utils.deepClone(this.actor.system.characteristics),
      labels: this._getLabels(),
      movement: this._getMovementSpeed(this.actor.system),
      senses: this._getSenses(this.actor.system),
      // Effects: ACE_ActiveEffect.prepareActiveEffectCategories(this.actor.effects),
      warnings: foundry.utils.deepClone(this.actor._preparationWarnings),
      filters: this._filters,
      owner: this.actor.isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: this.actor.isOwner ? "editable" : "locked",
      isCharacter: false,
      isNPC: false,
      isReferee: game.user.isGM,
      isUnit: false,
      isVehicle: false,
      isWorld: false,
      config: CONFIG.ACE,
      rollData: this.actor.getRollData.bind(this.actor)
    };

    // Sort Owned Items
    context.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    // Labels and filters
    // this.labels = this.actor.labels || {};
    // this.filters = this._filters;

    // Characteristics
    for (const [c, chr] of Object.entries(context.system.characteristics)) {
      chr.label = CONFIG.ACE.characteristics[c];
    }

    // Skills
    // for ( const [s, skill] of Object.entries(context.system.skills ?? {}) ) {
    //   skill.characteristic = CONFIG.ACE.characteristicAbbreviations[skill.characteristic];
    //   skill.icon = this._getProficiencyIcon(skill.value);
    //   skill.label = CONFIG.ACE.skills[s]?.label;
    //   skill.baseValue = source.system.skills[s]?.value ?? 0;
    // }

    // Prepare owned items
    this._prepareItems(context);

    // Biography HTML enrichment
    context.biographyHTML = await TextEditor.enrichHTML(
      context.system.details.biography.value,
      {
        secrets: this.actor.isOwner,
        rollData: context.rollData,
        async: true,
        relativeTo: this.actor
      });

    // Console.log("A·C·E | ACE_ActorSheet.getData():", this, options, context);

    // Return data to the sheet
    return context;
  }

  /* -------------------------------------------- */

  /**
   * Prepare labels object for the context.
   * @param {object} systemData  System data for the Actor being prepared.
   * @returns {object}           Object containing various labels.
   * @protected
   */
  _getLabels(systemData) {
    const labels = { ...this.actor.labels };

    // // Currency Labels
    // labels.currencies = Object.entries(CONFIG.DND5E.currencies).reduce((obj, [k, c]) => {
    //   obj[k] = c.label;
    //   return obj;
    // }, {});

    // // Proficiency
    // labels.proficiency = game.settings.get("dnd5e", "proficiencyModifier") === "dice"
    //   ? `d${systemData.attributes.prof * 2}`
    //   : `+${systemData.attributes.prof}`;

    return labels;
  }

  /* -------------------------------------------- */

  /**
   * Prepare the display of movement speed data for the Actor.
   * @param {object} systemData               System data for the Actor being prepared.
   * @param {boolean} [largestPrimary=false]  Show the largest movement speed as "primary", otherwise show "walk".
   * @returns {{primary: string, special: string}}
   * @private
   */
  _getMovementSpeed(systemData, largestPrimary = false) {
    const movement = systemData.attributes.movement || {};

    // Prepare an array of available movement speeds
    let speeds = [
      [movement.burrow, `${game.i18n.localize("ACE.MoveBurrow")} ${movement.burrow}`],
      [movement.climb, `${game.i18n.localize("ACE.MoveClimb")}  ${movement.climb}`],
      [movement.fly, `${game.i18n.localize("ACE.MoveFly")} ${movement.fly}${movement.hover ? ` (${game.i18n.localize("ACE.MoveHover")})` : ""}`],
      [movement.swim, `${game.i18n.localize("ACE.MoveSwim")}   ${movement.swim}`]
    ];
    if (largestPrimary) {
      speeds.push([movement.walk, `${game.i18n.localize("ACE.MoveWalk")} ${movement.walk}`]);
    }

    // Filter and sort speeds on their values
    speeds = speeds.filter(s => !!s[0]).sort((a, b) => b[0] - a[0]);

    // Case 1: Largest as primary
    if (largestPrimary) {
      let primary = speeds.shift();
      return {
        primary: `${primary ? primary[1] : "0"} ${movement.units}`,
        special: speeds.map(s => s[1]).join(", ")
      };
    }
    // Case 2: Walk as primary
    else {
      return {
        primary: `${movement.walk || 0} ${movement.units}`,
        special: speeds.length ? speeds.map(s => s[1]).join(", ") : ""
      };
    }
  }

  /* -------------------------------------------- */

  /**
   * Prepare senses object for display.
   * @param {object} systemData  System data for the Actor being prepared.
   * @returns {object}           Senses grouped by key with localized and formatted string.
   * @protected
   */
  _getSenses(systemData) {
    const senses = systemData.attributes.senses ?? {};
    const tags = {};
    for ( let [k, label] of Object.entries(CONFIG.ACE.senses) ) {
      const v = senses[k] ?? 0;
      if ( v === 0 ) continue;
      tags[k] = `${game.i18n.localize(label)} ${v} ${senses.units}`;
    }
    if ( senses.special ) tags.special = senses.special;
    return tags;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async activateEditor(name, options={}, initialContent="") {
    options.relativeLinks = true;
    return super.activateEditor(name, options, initialContent);
  }

  /* --------------------------------------------- */
  /*  Property Attribution                         */
  /* --------------------------------------------- */

  /**
   * Prepare the data structure for items which appear on the actor sheet.
   * Each subclass overrides this method to implement type-specific logic.
   * @protected
   */
  _prepareItems() {}

  /* -------------------------------------------- */

  /**
   * Determine whether an Owned Item will be shown based on the current set of filters.
   * @param {object[]} items       Copies of item data to be filtered.
   * @param {Set<string>} filters  Filters applied to the item list.
   * @returns {object[]}             Subset of input items limited by the provided filters.
   * @private
   */
  _filterItems(items, filters) {
    return items.filter(item => {
      // Action usage
      for (let f of ["action", "bonus", "reaction"]) {
        if (filters.has(f) && item.system.activation?.type !== f) return false;
      }

      // Equipment-specific filters
      if (filters.has("equipped") && (item.system.equipped !== true) ) return false;
      return true;
    });
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);

    // Activate Item Filters
    const filterLists = html.find(".filter-list");
    filterLists.each(this._initializeFilterItemList.bind(this));
    filterLists.on("click", ".filter-item", this._onToggleFilter.bind(this));

    // Item summaries
    html
      .find(".item .item-name.rollable h4")
      .click(event => this._onItemSummary(event));

    // View Item Sheets
    html.find(".item-edit").click(this._onItemEdit.bind(this));

    // Editable Only Listeners
    if (this.isEditable) {
      // Input focus and update
      const inputs = html.find("input");
      inputs.focus(ev => ev.currentTarget.select());
      inputs.addBack().find('[type="number"]').change(this._onChangeInputDelta.bind(this));

      // Html.find(".trait-selector").click(this._onTraitSelector.bind(this));

      // Configure Special Flags
      html.find(".config-button").click(this._onConfigMenu.bind(this));

      // Owned Item management
      html.find(".item-create").click(this._onItemCreate.bind(this));
      html.find(".item-delete").click(this._onItemDelete.bind(this));
      html.find(".item-uses input").click(event => event.target.select()).change(this._onUsesChange.bind(this));

      // Active Effect management
      html.find(".effect-control").click(event => ACE_ActiveEffect.onManageActiveEffect(event, this.actor));
    }

    // Owner Only Listeners
    if (this.actor.isOwner) {
      // Characteristic Checks
      html.find(".characteristic-name").click(this._onRollCharacteristicCheck.bind(this));

      // Roll Skill Checks
      html.find(".skill-name").click(this._onRollSkillCheck.bind(this));

      // Item Rolling
      html.find(".item .item-recharge").click(event => this._onItemRecharge(event));
      html.find(".rollable .item-image").click(event => this._onItemUse(event));
      html.find(".rollable .dodge").click(event => this._onDodgeClick(event));
      html.find(".rollable .improvised").click(event => this._onImprovisedClick(event));
      html.find(".rollable .unarmed").click(event => this._onUnarmedClick(event));
    }

    // Otherwise remove rollable classes
    else {
      html.find(".rollable").each((i, el) => el.classList.remove("rollable"));
    }

    // Handle default listeners last so system listeners are triggered first
    super.activateListeners(html);
  }

  /* -------------------------------------------- */

  /**
   * Initialize Item list filters by activating the set of filters which are currently applied
   * @param {number} i  Index of the filter in the list.
   * @param {HTML} ul   HTML object for the list item surrounding the filter.
   * @private
   */
  _initializeFilterItemList(i, ul) {
    const set = this._filters[ul.dataset.filter];
    const filters = ul.querySelectorAll(".filter-item");
    for (let li of filters) {
      if (set.has(li.dataset.filter)) li.classList.add("active");
    }
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /**
   * Handle input changes to numeric form fields, allowing them to accept delta-typed inputs
   * @param {Event} event  Triggering event.
   * @private
   */
  _onChangeInputDelta(event) {
    const input = event.target;
    const value = input.value;
    if ( ["+", "-"].includes(value[0]) ) {
      let delta = parseFloat(value);
      input.value = foundry.utils.getProperty(this.actor, input.name) + delta;
    }
    else if ( value[0] === "=" ) input.value = value.slice(1);
  }

  /**
   * Handle spawning the TraitSelector application which allows a checkbox of multiple trait options.
   * @param {Event} event   The click event which originated the selection.
   * @private
   */
  _onConfigMenu(event) {
    event.preventDefault();
    const button = event.currentTarget;
    let app;
    switch (button.dataset.action) {
      case "armor":
        app = new ActorArmorConfig(this.actor);
        break;
      case "movement":
        app = new ActorMovementConfig(this.actor);
        break;
    }
    app?.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Change the uses amount of an Owned Item within the Actor.
   * @param {Event} event        The triggering click event.
   * @returns {Promise<Item5e>}  Updated item.
   * @private
   */
  async _onUsesChange(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    const uses = Math.clamped(0, parseInt(event.target.value), item.system.uses.max);
    event.target.value = uses;
    return item.update({"system.uses.value": uses});
  }

  /* -------------------------------------------- */

  /**
   * Handle using an item from the Actor sheet, obtaining the Item instance, and dispatching to its use method.
   * @param {Event} event  The triggering click event.
   * @returns {Promise}    Results of the usage.
   * @protected
   */
  _onItemUse(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);

    console.log("A·C·E | ACE_ActorSheet._onItemUse():", this, event);

    if (item) return item.use();

    return "";
  }

  /* -------------------------------------------- */

  /**
   * Handle the actor performing an umarmed attack.
   * @param {Event} event  The triggering click event.
   * @returns {Promise}    Results of the usage.
   * @protected
   */
  async _onUnarmedClick(event) {
    event.preventDefault();

    console.log("A·C·E | ACE_ActorSheet._onUnarmedClick():", this, event);

    return "";
  }

  /* -------------------------------------------- */

  /**
   * Handle toggling and items expanded description.
   * @param {Event} event   Triggering event.
   * @private
   */
  async _onItemSummary(event) {
    event.preventDefault();
    const li = $(event.currentTarget).parents(".item");
    const item = this.actor.items.get(li.data("item-id"));
    const chatData = await item.getChatData({ secrets: this.actor.isOwner });

    // Toggle summary
    if (li.hasClass("expanded")) {
      let summary = li.children(".item-summary");
      summary.slideUp(200, () => summary.remove());
    } else {
      let div = $(
        `<div class="item-summary">${chatData.description.value}</div>`
      );
      let props = $('<div class="item-properties"></div>');
      chatData.properties.forEach(p =>
        props.append(`<span class="tag">${p}</span>`)
      );
      div.append(props);
      li.append(div.hide());
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }

  /* -------------------------------------------- */

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset.
   * @param {Event} event          The originating click event.
   * @returns {Promise<ACE_Item[]>}  The newly created item.
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const type = header.dataset.type;
    const itemData = {
      name: game.i18n.format("ACE.ItemNew", {
        type: game.i18n.localize(`ACE.ItemType${type.capitalize()}`)
      }),
      type: type,
      data: foundry.utils.deepClone(header.dataset)
    };
    delete itemData.system.type;
    return this.actor.createEmbeddedDocuments("Item", [itemData]);
  }

  /* -------------------------------------------- */

  /**
   * Handle editing an existing Owned Item for the Actor.
   * @param {Event} event    The originating click event.
   * @returns {ACE_ItemSheet}  The rendered item sheet.
   * @private
   */
  _onItemEdit(event) {
    event.preventDefault();
    const li = event.currentTarget.closest(".item");
    const item = this.actor.items.get(li.dataset.itemId);
    return item.sheet.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Handle deleting an existing Owned Item for the Actor.
   * @param {Event} event  The originating click event.
   * @returns {Promise<ACE_Item>|undefined}  The deleted item if something was deleted.
   * @private
   */
  _onItemDelete(event) {
    event.preventDefault();
    const li = event.currentTarget.closest(".item");
    const item = this.actor.items.get(li.dataset.itemId);
    if (!item) return;

    // If item has advancement, handle it separately
    // ...

    return item.delete();
  }

  /* -------------------------------------------- */

  /**
   * Handle displaying the property attribution tooltip when a property is hovered over.
   * @param {Event} event   The originating mouse event.
   * @private
   */
  async _onPropertyAttribution(event) {
    const existingTooltip = event.currentTarget.querySelector("div.tooltip");
    const property = event.currentTarget.dataset.property;
    if ( existingTooltip || !property ) return;
    const rollData = this.actor.getRollData({ deterministic: true });
    let attributions;
    switch ( property ) {
      case "attributes.armor":
        attributions = this._prepareArmorClassAttribution(rollData); break;
    }
    if ( !attributions ) return;
    const html = await new PropertyAttribution(this.actor, attributions, property).renderTooltip();
    event.currentTarget.insertAdjacentElement("beforeend", html[0]);
  }

  /* -------------------------------------------- */

  /**
   * Handle rolling an Characteristic test or saving throw.
   * @param {Event} event      The originating click event.
   * @private
   */
  _onRollCharacteristicCheck(event) {
    event.preventDefault();
    let characteristic = event.currentTarget.parentElement.dataset.characteristic;
    this.actor.rollCharacteristic(characteristic, { event: event });
  }

  /* -------------------------------------------- */

  /**
   * Handle rolling a Skill check.
   * @param {Event} event      The originating click event.
   * @returns {Promise<Roll>}  The resulting roll.
   * @private
   */
  _onRollSkillCheck(event) {
    event.preventDefault();
    const skillId = event.currentTarget.closest("[data-skill-id]").dataset.skillId;

    console.log("A·C·E | ACE_ActorSheet._onRollSkillCheck():", this, event, skillId);

    return this.object.rollSkill(skillId, { event: event });
  }

  /* -------------------------------------------- */

  /**
   * Handle toggling of filters to display a different set of owned items.
   * @param {Event} event     The click event which triggered the toggle.
   * @returns {ACE_ActorSheet}  This actor sheet with toggled filters.
   * @private
   */
  _onToggleFilter(event) {
    event.preventDefault();
    const li = event.currentTarget;
    const set = this._filters[li.parentElement.dataset.filter];
    const filter = li.dataset.filter;
    if ( set.has(filter) ) set.delete(filter);
    else set.add(filter);
    return this.render();
  }
}
