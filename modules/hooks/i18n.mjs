import * as utils from "../utils/_module.mjs";

/* -------------------------------------------- */
/*  Foundry VTT I18n Initialization             */
/* -------------------------------------------- */

/**
 * Perform one-time pre-localization and sorting of some configuration objects
 */
export default function() {
  Hooks.once("i18nInit", () => utils.performPreLocalization(CONFIG.ACE));
}
