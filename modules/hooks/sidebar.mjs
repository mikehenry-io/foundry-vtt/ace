import ACE_CharacterCreator from "../apps/char-gen.mjs";

/**
 *
 */
export default function() {
  Hooks.on("renderSidebarTab", async (app, html) => {

    if (app.options.id === "actors") {

      let create_character_button = $(`
        <div class='flexrow'>
           <button class='create-character'>
             <i class='fas fa-user-plus'></i>
             ${game.i18n.localize("ACE.GenerateCharacter")}
           </button>
         </div>
      `);

      // --
      create_character_button.click(ev => {

        new Dialog({
          title: game.i18n.localize("ACE.GenerateCharacter"),
          content: `<p>${game.i18n.localize("DIALOG.GenerateCharacter")}</p>`,
          buttons: {
            yes: {
              label: game.i18n.localize("Yes"),
              callback: dlg => {
                ui.sidebar.activateTab("chat");
                CONFIG.Actor.documentClass.create(
                  {type: "character", name: "New Character"},
                  {renderSheet: true}
                );
                ACE_CharacterCreator.start();
                game.ACE.creator.speciesStage();
              }
            },
            no: {
              label: game.i18n.localize("No")
            }
          }
        }).render(true);

      });

      // --
      create_character_button.insertAfter(html.find(".header-actions"));

    }
  });
}
