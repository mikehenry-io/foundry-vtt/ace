/* -------------------------------------------- */
/*  Foundry VTT Setup                           */
/* -------------------------------------------- */

/**
 * Perform one-time pre-localization and sorting of some configuration objects
 */
export default function() {
  Hooks.once("setup", function() {
    CONFIG.ACE.trackableAttributes = expandAttributeList(CONFIG.ACE.trackableAttributes);
    CONFIG.ACE.consumableResources = expandAttributeList(CONFIG.ACE.consumableResources);
  });

  /* --------------------------------------------- */

  /**
   * Expand a list of attribute paths into an object that can be traversed.
   * @param {string[]} attributes  The initial attributes configuration.
   * @returns {object}  The expanded object structure.
   */
  function expandAttributeList(attributes) {
    return attributes.reduce((obj, attr) => {
      foundry.utils.setProperty(obj, attr, true);
      return obj;
    }, {});
  }
}
