import * as chat from "../chat.mjs";
import * as items from "../items/_module.mjs";

/* -------------------------------------------- */
/*  Canvas Initialization                       */
/* -------------------------------------------- */

/**
 *
 */
export default function() {
  Hooks.on("renderChatMessage", chat.onRenderChatMessage);
  Hooks.on("getChatLogEntryContext", chat.addChatMessageContextOptions);

  Hooks.on("renderChatLog", (app, html, data) => items.ACE_Item.chatListeners(html));
  Hooks.on("renderChatPopout", (app, html, data) => items.ACE_Item.chatListeners(html));
}
