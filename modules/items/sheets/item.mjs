// Import ACE_ActiveEffect from "../../documents/active-effect.mjs";

/**
 * Override and extend the core ItemSheet implementation to handle specific item types.
 * @extends {ItemSheet}
 */
export class ACE_ItemSheet extends ItemSheet {

  /* -------------------------------------------- */

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "item"],
      dragDrop: [{
        dragSelector: "[data-effect-id]",
        dropSelector: ".effects-list"
      }],
      resizable: true,
      scrollY: [".tab.details"],
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "description"
      }],
      height: 400,
      width: 560
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get template() {
    return `systems/ACE/templates/items/${this.item.type}.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const context = await super.getData(options);
    const item = context.item;
    const source = item.toObject();
    const isMountable = this._isItemMountable(item);

    foundry.utils.mergeObject(context, {
      source: source.system,
      system: item.system,
      labels: item.labels,
      isEmbedded: item.isEmbedded,

      // Item Type, Status, and Details
      itemType: game.i18n.localize(`ITEM.Type${item.type.titleCase()}`),
      itemStatus: this._getItemStatus(),
      itemProperties: this._getItemProperties(),
      // BaseItems: await this._getItemBaseTypes(),
      isObject: item.system.hasOwnProperty("quantity"),

      // Enrich HTML description
      descriptionHTML: await TextEditor.enrichHTML(item.system.description.value, {
        secrets: item.isOwner,
        async: true,
        relativeTo: this.item
      }),

      // Action Details
      hasAttackRoll: item.hasAttack,
      isHealing: item.system.actionType === "healing",
      isLine: ["line", "wall"].includes(item.system.target?.type),

      // Vehicles
      isCrewed: item.system.activation?.type === "crew",
      isMountable,

      // Armor Class
      isArmor: item.isArmor,
      hasAC: item.isArmor || isMountable,
      hasDexModifier: item.isArmor && (item.system.armor?.type !== "shield")

      // Prepare Active Effects
      // effects: ACE_ActiveEffect.prepareActiveEffectCategories(item.effects)
    });

    // Potential consumption targets
    context.characteristicConsumptionTargets = this._getItemConsumptionTargets(item);

    // Set up config with proper spell components
    context.config = foundry.utils.mergeObject(CONFIG.ACE, {
    }, {inplace: false});

    // Console.log("A·C·E | ACE_ItemSheet.getData(%o):\n%o", options, context);

    return context;
  }

  /* -------------------------------------------- */

  /**
   * Get the base weapons and tools based on the selected type.
   * @returns {Promise<object>}  Object with base items for this type formatted for selectOptions.
   * @protected
   */
  // async _getItemBaseTypes() {
  //   const type = this.item.type === "equipment" ? "armor" : this.item.type;
  //   const baseIds = CONFIG.ACE[`${type}Ids`];
  //   if ( baseIds === undefined ) return {};

  //   const typeProperty = type === "armor" ? "armor.type" : `${type}Type`;
  //   const baseType = foundry.utils.getProperty(this.item.system, typeProperty);

  //   const items = {};
  //   for ( const [name, id] of Object.entries(baseIds) ) {
  //     const baseItem = await ProficiencySelector.getBaseItem(id);
  //     if ( baseType !== foundry.utils.getProperty(baseItem.system, typeProperty) ) continue;
  //     items[name] = baseItem.name;
  //   }
  //   return Object.fromEntries(Object.entries(items).sort((lhs, rhs) => lhs[1].localeCompare(rhs[1])));
  // }

  /* -------------------------------------------- */

  /**
   * Get the valid item consumption targets which exist on the actor
   * @returns {Object<string>}   An object of potential consumption targets
   * @private
   */
  _getItemConsumptionTargets() {
    const consume = this.item.system.consume || {};
    if ( !consume.type ) return [];
    const actor = this.item.actor;
    if ( !actor ) return {};

    // Ammunition
    if ( consume.type === "ammunition" ) {
      return actor.itemTypes.consumable.reduce((ammo, i) => {
        if ( i.system.consumableType === "ammunition" ) ammo[i.id] = `${i.name} (${i.system.quantity})`;
        return ammo;
      }, {[this.item.id]: `${this.item.name} (${this.item.system.quantity})`});
    }
    else return {};
  }

  /* -------------------------------------------- */

  /**
   * Get the text item status which is shown beneath the Item type in the top-right corner of the sheet.
   * @returns {string|null}  Item status string if applicable to item's type.
   * @private
   */
  _getItemStatus() {
    switch (this.item.type) {
      case "equipment":
      case "weapon":
        return game.i18n.localize(this.item.system.equipped ? "ACE.Equipped" : "ACE.Unequipped");
    }
  }

  /* -------------------------------------------- */

  /**
   * Get the Array of item properties which are used in the small sidebar of the description tab.
   * @returns {string[]}   List of property labels to be shown.
   * @private
   */
  _getItemProperties() {
    const props = [];
    const labels = this.item.labels;
    switch ( this.item.type ) {
      case "equipment":
        props.push(CONFIG.ACE.equipmentTypes[this.item.system.armor.type]);
        if ( this.item.isArmor || this._isItemMountable(this.item) ) props.push(labels.armor);
        break;
      case "weapon":
        for ( const [k, v] of Object.entries(this.item.system.properties) ) {
          if ( v === true ) props.push(CONFIG.ACE.weaponProperties[k]);
        }
        break;
    }

    // Action type
    if ( this.item.system.actionType ) {
      props.push(CONFIG.ACE.itemActionTypes[this.item.system.actionType]);
    }

    // Action usage
    if ( (this.item.type !== "weapon") && !foundry.utils.isEmpty(this.item.system.activation) ) {
      props.push(labels.activation, labels.range, labels.target, labels.duration);
    }
    return props.filter(p => !!p);
  }

  /* -------------------------------------------- */

  /**
   * Is this item a separate large object like a siege engine or vehicle component that is
   * usually mounted on fixtures rather than equipped, and has its own AC and HP.
   * @param {object} item  Copy of item data being prepared for display.
   * @returns {boolean}    Is item siege weapon or vehicle equipment?
   * @private
   */
  _isItemMountable(item) {
    return ((item.type === "weapon") && (item.system.weaponType === "siege"))
      || (item.type === "equipment" && (item.system.armor.type === "vehicle"));
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  setPosition(position={}) {
    if ( !(this._minimized || position.height) ) {
      position.height = (this._tabs[0].active === "details") ? "auto" : this.options.height;
    }
    return super.setPosition(position);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async activateEditor(name, options={}, initialContent="") {
    options.relativeLinks = true;
    options.plugins = {
      menu: ProseMirror.ProseMirrorMenu.build(ProseMirror.defaultSchema, {
        compact: true,
        destroyOnSave: true,
        onSave: () => this.saveEditor(name, {remove: true})
      })
    };
    return super.activateEditor(name, options, initialContent);
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  activateListeners(html) {
    super.activateListeners(html);
    if (this.isEditable) {
      html.find(".damage-control").click(this._onDamageControl.bind(this));
      // Html.find(".trait-selector").click(this._onConfigureTraits.bind(this));
      // html.find(".effect-control").click(ev => {
      //   if (this.item.isOwned) return ui.notifications.warn("Managing Active Effects within an Owned Item is not currently supported and will be added in a subsequent update.");
      //   ActiveEffect5e.onManageActiveEffect(ev, this.item);
      // });
    }
  }

  /* -------------------------------------------- */

  /**
   * Add or remove a damage part from the damage formula.
   * @param {Event} event             The original click event.
   * @returns {Promise<ACE_Item>|null}  Item with updates applied.
   * @private
   */
  async _onDamageControl(event) {
    event.preventDefault();
    const a = event.currentTarget;

    // Add new damage component
    if ( a.classList.contains("add-damage") ) {
      await this._onSubmit(event);  // Submit any unsaved changes
      const damage = this.item.system.damage;
      return this.item.update({ "system.damage.parts": damage.parts.push({ roll: "", type: "" })});
    }

    // Remove a damage component
    if ( a.classList.contains("delete-damage") ) {
      await this._onSubmit(event);  // Submit any unsaved changes
      const li = a.closest(".damage-part");
      const damage = foundry.utils.deepClone(this.item.system.damage);
      damage.parts.splice(Number(li.dataset.damagePart), 1);
      return this.item.update({"system.damage.parts": damage.parts});
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragStart(event) {
    const li = event.currentTarget;
    if ( event.target.classList.contains("content-link") ) return;

    // Create drag data
    let dragData;

    // Active Effect
    if ( li.dataset.effectId ) {
      const effect = this.item.effects.get(li.dataset.effectId);
      dragData = effect.toDragData();
    }

    if ( !dragData ) return;

    // Set data transfer
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDrop(event) {
    const data = TextEditor.getDragEventData(event);
    const item = this.item;

    /**
     * A hook event that fires when some useful data is dropped onto an ACE_ItemSheet.
     * @function ACE.dropItemSheetData
     * @memberof hookEvents
     * @param {ACE_Item} item                  The ACE_Item
     * @param {ACE_ItemSheet} sheet            The ACE_ItemSheet application
     * @param {object} data                  The data that has been dropped onto the sheet
     * @returns {boolean}                    Explicitly return `false` to prevent normal drop handling.
     */
    const allowed = Hooks.call("ACE.dropItemSheetData", item, this, data);
    if ( allowed === false ) return;

    switch ( data.type ) {
      case "ActiveEffect":
        return this._onDropActiveEffect(event, data);
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle the dropping of ActiveEffect data onto an Item Sheet
   * @param {DragEvent} event                  The concluding DragEvent which contains drop data
   * @param {object} data                      The data transfer extracted from the event
   * @returns {Promise<ActiveEffect|boolean>}  The created ActiveEffect object or false if it couldn't be created.
   * @protected
   */
  async _onDropActiveEffect(event, data) {
    const effect = await ActiveEffect.implementation.fromDropData(data);
    if ( !this.item.isOwner || !effect ) return false;
    if ( (this.item.uuid === effect.parent.uuid) || (this.item.uuid === effect.origin) ) return false;
    return ActiveEffect.create({
      ...effect.toObject(),
      origin: this.item.uuid
    }, {parent: this.item});
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onSubmit(...args) {
    if ( this._tabs[0].active === "details" ) this.position.height = "auto";
    await super._onSubmit(...args);
  }
}
