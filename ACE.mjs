/**
 * The Cepheus Engine game system for Foundry Virtual Tabletop
 * A system for playing Cepheus Engine and other 2D6 role-playing games.
 * Author: Mike Henry
 * Software License: MIT
 * Content License: https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf
 * Repository: https://gitlab.com/mikehenry-io/foundry-vtt/ace
 * Issue Tracker: https://gitlab.com/mikehenry-io/foundry-vtt/ace/issues
 */

// Import configuration
import ACE from "./modules/config.mjs";

// Import submodules
import * as actors from "./modules/actors/_module.mjs";
import * as apps from "./modules/apps/_module.mjs";
import * as hooks from "./modules/hooks/_module.mjs";
import * as items from "./modules/items/_module.mjs";
import * as migration from "./modules/migration.mjs";
import * as rolls from "./modules/rolls/_module.mjs";
import * as utils from "./modules/utils/_module.mjs";

/* -------------------------------------------- */
/*  Define Module Structure                     */
/* -------------------------------------------- */

globalThis.ACE = {
  actors,
  apps,
  config: ACE,
  hooks,
  items,
  migration,
  rolls,
  utils
};

// Export bundled modules
export { ACE, actors, apps, hooks, items, migration, rolls, utils };
